// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/


// New features to JS

// Exponent Operator

console.log("ESG Updates");
console.log("Exponent Operatr");

// math obj met
const firstNum = Math.pow(8, 3); // 8 * 8 * 8
console.log(firstNum);

// expo opera
const secondNum = 8 ** 3; // 8 ^ 3
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("------------------");
console.log("Template Literals");

let name = "John";

// using template literal using backtick ``
let message = (`Hello ${name}! Welcome to programming!`);

console.log(message);
console.log("------------------");


// Array Destructuring
// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/

console.log("Pre-Array Destructuring");

const fullName = ["Juan", "Dela", "Cruz"];
// pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log("Real Array Destructuring");
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(lastName);

console.log("------------------");

console.log("Object Destructuring");



const person = {
	givenName: "Jane",
	maidenName: "Dela",
	surName: "Cruz"
}


console.log(person.givenName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you again`);


// Object Destructuring

const{givenName, maidenName, surName} = person;
console.log(givenName);
console.log(maidenName);

console.log(`Hello ${givenName} ${maidenName} ${surName}! It's good to see you again`);

// [SECTION] Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of code.
	- This will work with "function expression." (s17)

	example of funcExpression

	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	}
	funcExpression();


*/

/*
Syntax

let/const variableName = (parameter) => {
	// code to execute;
}

//invocation
variableName(argument);
*/


console.log("------------------");
console.log("Arrow Functions");




const hello = () =>{
	console.log("HELLO POTA KA");
}

hello();

const students = ["Joy", "Jones", "Julay"];

students.forEach(function(student){
	console.log(`${student} is a student`)
});

// forEach method with the use of arrow function

students.forEach((student)=>
console.log(`${student} is a student.`)
);

// an0nymous function - a func that has no func name

console.log("------------------");
console.log("IMPLICIT RETURN using arrow functions");

// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
const add = (x, y) => x + y;

let total = add(1,2);

console.log(total);




console.log("------------------");



// DEFAULT FUNCTION ARGUMENT VALUE
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good MORNING, ${name}`;
console.log(greet());
console.log(greet("JOHN"));

console.log("-----------");
console.log("=> Class-Based Object Blueprint: ");
// [SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.


// the constructor is a special method of a class for creating.initialzing object for  that class.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

let newCar = new Car("VOVO", "DUMPTRUCK", 1931);
console.log(newCar);

const myCar = new Car();

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar['year'] =  2021;

console.log(myCar);